#include <array>
#include <cassert>
#include <iostream>
#include <string>
#include <vector>

class TuringMachine {
public:
    explicit TuringMachine(const std::string& tape) : tape_(tape) { }

    void run() {
        while (state_ != STOP)
            transition();
    }

    const std::string& getTape() const { return tape_; }

private:
    void checkTape() {
        if (tape_.size() == (size_t)pos_)
            tape_.push_back('0');  // increase tape length
    }

    unsigned readTape() {
        checkTape();
        return tape_[pos_] - '0';
    }

    void writeTape(unsigned i) {
        checkTape();
        tape_[pos_] = '0' + i;
    }

    void transition() {
        int c = readTape();
        const auto& transition = transitions_[state_][c];
        writeTape(transition.write);
        pos_ += transition.dir;
        assert(pos_ >= 0);
        state_ = transition.next_state;
    }

    std::string tape_;
    int pos_ = 0;
    enum state { E1, E2, E3, E4, E5, STATE_COUNT, STOP } state_ = E1;

    enum direction { LEFT = -1, RIGHT = +1 };
    struct Transition {
        unsigned write;
        direction dir;
        state next_state;
    };

    // State machine definition
    std::array<std::array<Transition, 2>, STATE_COUNT> transitions_ = {{
        {{ // E1
           /* 0 */ Transition{0, LEFT,  STOP},
           /* 1 */ Transition{0, RIGHT, E2},
        }},
        {{ // E2
           /* 0 */ Transition{0, RIGHT, E3},
           /* 1 */ Transition{1, RIGHT, E2},
        }},
        {{ // E3
           /* 0 */ Transition{1, LEFT,  E4},
           /* 1 */ Transition{1, RIGHT, E3},
        }},
        {{ // E4
           /* 0 */ Transition{0, LEFT,  E5},
           /* 1 */ Transition{1, LEFT,  E4},
        }},
        {{ // E5
           /* 0 */ Transition{1, RIGHT, E1},
           /* 1 */ Transition{1, LEFT,  E5},
        }}
    }};
};

int main() {
    std::string tape{"111"};
    TuringMachine M(tape);
    M.run();
    std::cout << M.getTape() << std::endl;

    return 0;
}
